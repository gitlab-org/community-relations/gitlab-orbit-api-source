# frozen_string_literal: true

require 'httparty'

class OrbitApiClient
  include HTTParty

  base_uri 'https://app.orbit.love/api/v1/gitlab-0105df'

  def initialize
    api_key = ENV['ORBIT_API_TOKEN']
    @default_headers = {
      'Content-Type' => 'application/json',
      'Authorization' => "Bearer #{api_key}"
    }
  end

  def get_actvitiy_types
    self.class.get('/activity_types', headers: @default_headers)
  end

  def create_or_update_member(username, email, name, avatar_url, bio, linkedin, twitter, discord, organization, job_title)
    puts 'create_or_update_member'

    body = {
      member: {
        bio: bio,
        company: organization,
        title: job_title,
        name: name,
        twitter: twitter,
        email: email,
        linkedin: linkedin,
      },
      identity: {
        name: name,
        source: 'gitlab',
        username: username,
        email: email,
        url: "https://gitlab.com/#{username}"
      } }.to_json
    puts body
    response = post_with_rate_limit('/members', body)
    puts response
  end

  def add_merge_request_activity(username, id, merged_at, title, link)
    puts 'add_merge_request_activity'
    body = {
      identity: {
        source: 'gitlab',
        username: username,
        url: "https://gitlab.com/#{username}"
      },
      activity: {
        title: "Merge request #{id} merged",
        activity_type_key: 'gitlab:merge_request:merged',
        key: id,
        occurred_at: merged_at,
        link: link,
        description: title
      } }.to_json
    puts body
    response = post_with_rate_limit('/activities', body)
    puts response
  end

  def post_with_rate_limit(endpoint, body)
    self.class.post(endpoint, headers: @default_headers, body: body)
  rescue HTTParty::ResponseError => error
    puts error
    puts 'Waiting for 60 seconds...'
    sleep(60)
    self.class.post('/activities', headers: @default_headers, body: body)
  end
end
