# frozen_string_literal: true

require 'gitlab'

require_relative 'orbit_api_client'

ENDPOINT = 'https://gitlab.com/api/v4'
USER_AGENT = 'GitLab Commonroom API Source'
API_TOKEN = ENV['GITLAB_API_TOKEN']
GROUP_ID = 9970
LABELS = ['Community contribution']

Gitlab.configure do |config|
  config.endpoint = ENDPOINT
  config.user_agent = USER_AGENT
  config.private_token = API_TOKEN
end

page = 1
mrs = []
authors = Hash.new

loop do
  merge_requests = Gitlab.group_merge_requests(GROUP_ID,
    labels: LABELS,
    state: 'merged',
    per_page: 100,
    page: page,
    order_by: 'created_at')
  break if merge_requests.empty?

  puts "Got #{merge_requests.count} merge requests..."
  merge_requests.each do |mr|
    next if mr.author.username.include?('_bot_')
    authors[mr.author.id] = nil
    mrs << { author_id: mr.author.id, id: mr.id, merged_at: mr.merged_at, created_at: mr.created_at, title: mr.title, link: mr.web_url }
  end

  puts mrs.last[:created_at]
  break if mrs.last[:created_at] < '2022-04-01T00:00:00.000Z'
  page += 1
end

api_client = OrbitApiClient.new

authors.each do |id, email|
  sleep(2)
  author = Gitlab.user(id)
  authors[id] = author.username
  api_client.create_or_update_member(author.username,
    author.public_email,
    author.name,
    author.avatar_url,
    author.bio,
    author.linkedin,
    author.twitter,
    author.discord,
    author.organization,
    author.job_title)
rescue => error
  puts error
  puts 'Waiting for 60 seconds...'
  sleep(60)
  author = Gitlab.user(id)
  authors[id] = author.username
  api_client.create_or_update_member(author.username,
    author.public_email,
    author.name,
    author.avatar_url,
    author.bio,
    author.linkedin,
    author.twitter,
    author.discord,
    author.organization,
    author.job_title)
end

mrs.each do |mr|
  api_client.add_merge_request_activity(authors[mr[:author_id]],
    mr[:id],
    mr[:merged_at],
    mr[:title],
    mr[:link])
end
